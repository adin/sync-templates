# About

The goal of the `sync.py` tool is to allow the syncronization of issue-templates accross several projects within a project.

As it is for now, GitLab doesn't support replacation of templates. But, most of the time, one has the necessity to reuse some templates accross several projects, and the task of maintaining them is daunting. Thus, this scripts aims to simplify that task.

# Usage

The basic usage is to give a `group-name` and/or a `project-name` and that will sync the default folder with the templates (`./gitlab-issue-templates`) into the default template folder in the remote (`.gitlab/issue_templates`)

```bash
python3 sync.py -g group-name -p project-name

```

or update all the projects within the `group-name`

```bash
python3 sync.py -g group-name
```

For the full usage 

```bash
python3 sync.py -h
```

# Setup

You need to setup the a configuration file, by default `./gl.cfg`, that can be set with the `--config` option.

The configuration file is for the [`gitlab` API interface](http://python-gitlab.readthedocs.io/en/stable/cli.html#configuration). A minimal example is

```.cfg
[global]
default = gitlab.com
ssl_verify = true
timeout = 5

[gitlab.com]
url = https://gitlab.com
private_token = your-private-token-here
```