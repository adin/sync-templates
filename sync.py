# Script to sync templates of a group of repositories in Gitlab

# install 'pip install python-gitlab' 
# or latesat from repo 'pip install git+git://github.com/gpocentek/python-gitlab.git'
# documentation http://python-gitlab.readthedocs.io/en/stable/api-usage.html#gitlab-gitlab-class
import gitlab

from os import listdir
from os.path import isfile, join
import argparse

parser = argparse.ArgumentParser(description="This syncronizes the files in a given directory to the templates in the remote.",
  epilog='''For example, suppose you want to sync the files in "./templates" into all your projects in "my-group", then you do "%(prog)s -g my-group -d ./templates", or you can restrict it by doing a single project, "my-project", in that group, with "%(prog)s -g my-group -p my-project -d ./templates".''')
parser.add_argument('-g', '--group', help='group key to search', default='')
parser.add_argument('-p', '--project', help='project key to search', default='')
parser.add_argument('-t', '--template', help='directory of the templates in the remote', default='.gitlab/issue_templates')
parser.add_argument('-d', '--directory', help='directory of the templates in the local machine', default='./gitlab-issue-templates')
parser.add_argument('-b', '--branch', help='the branch to work on', default='master')
parser.add_argument('-m', '--message', help='the commit message', default='Update issue templates')
parser.add_argument('-dm', '--delete-missing', help='delete files that are not present on the sync folder', type=bool, default=False)
parser.add_argument('-c', '--config', help='the path of the configuration file', default='./gl.cfg')

args = parser.parse_args()

group_key = args.group
project_key = args.project
templates_dir = args.template
local_dir = args.directory
branch = args.branch
commit_message = args.message
config_file = args.config
delete_missing_files = args.delete_missing

# private token auth
# gl = gitlab.Gitlab('http://gitlab.com',TOKEN)
# or with config file
gl = gitlab.Gitlab.from_config('gitlab.com', [config_file])

projects = []
# if there is not a group key
if not group_key:
  # get the project according to the key given (subset or all) from root
  if not project_key:
    print('Not given a project nor a group to work with. Are you sure you want to process all your projects in all your groups? Think again.')
    exit()
  else:
    projects = gl.projects.search(project_key)
    print('Processing a subset of projects on root (%s)' % project_key)
else:
  # find the group
  groups = gl.groups.search(group_key)
  print('Searching for %s:'%group_key)
  # for each group search for the projects
  for group in groups:
    print('gid: %s' % group.id)
    # get the project according to the key given (subset or all)
    if not project_key:
      # convert from GroupProject (returned by group.projects.list) to Project
      projects.extend([gl.projects.get(gp.id) for gp in group.projects.list()])
      print('Processing all projects on %s' % group_key)
    else:
      projects.extend( [gl.projects.get(gp.id) for gp in group.projects.list() if gp.name == project_key] )
      print('Processing a subset of projects on %s' % group_key)


# process all the projects
for project in projects:
  print('Processing files for %s (%s)' % (project.name, project.id) )
  # find the files in the templates folder to push to each repo
  local_files = [f for f in listdir(local_dir) if isfile(join(local_dir, f))];
  # lets push each file one at a time

  actions = []
  # get the list of existing templates to update
  try:
    existing_files = project.repository_tree(path=templates_dir, ref=branch)
  except:
    existing_files = []

  for file in local_files:      
    # get the data of the file
    stream = open("%s/%s"%(local_dir, file), 'r')
    data = stream.read()
    # verify if the file exists or not
    exist = [f for f in existing_files if f['name'] == file]

    # update the action according to the file status (create/update)
    if exist:
      print("  %s exist" % file)
      # Get the file data
      f = project.files.get(file_path='%s/%s'%(templates_dir, file), ref=branch)
      # Compare the decoded version of the file (bytes) with the encoded version of the string (bytes)
      # If they are equal pass this file
      if f.decode() == data.encode():
        action='none'
        print('  %s is up to date' % file)
        continue
      # Update the file
      else:
        action='update'
        print("  %s will be updated" % file)
    else:
      # As it doesn't exist create the file
      print("  %s does not exist" % file)
      action='create'
    # append the action to the list
    actions.append({
      'action': action,
      'file_path': '%s/%s' % (templates_dir, file),
      'content': data})
  
  if delete_missing_files:
    for repo_file in existing_files:
      # verify if the file in the repo exists on the syncing folder
      exist = [f for f in local_files if f == repo_file['name']]

      # delete if does not exist locally
      if not exist:
        print("  %s does not exist locally, it will be deleted in the repo" % repo_file['name'])
        actions.append({
          'action': 'delete',
          'file_path': '%s/%s' % (templates_dir, repo_file['name'])
        })

  # If there are actions for this repo, process them
  if actions:
    print("  Applying %s actions to project %s" % (len(actions), project.name) )
    # process all the actions in one commit
    f = project.commits.create({
        'branch_name': branch,
        'commit_message': commit_message,
        'actions': actions})
  else:
    print("  Project %s is up to date" % project.name)
    continue
